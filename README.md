# AzureWebapp

Azure CLI
login into to Azure "az login"
Set the subscription "az account set --subscription "<subscription name>"

Create your ASPNETCore Application
Navigate to the application directory where the files are, and type in the following command to create the application service within Azure 
"az webapp up --sku F1 --name h-webapp-2 --os-type windows --location "UK South" --resource-group <rg name>"

For further info use the links below

az webapp up [--app-service-environment]
             [--dryrun]
             [--html]
             [--ids]
             [--launch-browser]
             [--location]
             [--logs]
             [--name]
             [--os-type {Linux, Windows}]
             [--plan]
             [--resource-group]
             [--runtime]
             [--sku {B1, B2, B3, D1, F1, FREE, I1, I1v2, I2, I2v2, I3, I3v2, P1V2, P1V3, P2V2, P2V3, P3V2, P3V3, PC2, PC3, PC4, S1, S2, S3, SHARED}]
             [--subscription]

https://serverhealers.com/blog/deploy-an-asp-net-web-application-to-azure-app-service

https://docs.microsoft.com/en-us/cli/azure/webapp?view=azure-cli-latest#az_webapp_up
